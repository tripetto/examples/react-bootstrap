/** Import block packages */
import "@tripetto/block-calculator";
import "@tripetto/block-checkbox";
import "@tripetto/block-checkboxes";
import "@tripetto/block-device";
import "@tripetto/block-dropdown";
import "@tripetto/block-email";
import "@tripetto/block-evaluate";
import "@tripetto/block-hidden-field";
import "@tripetto/block-number";
import "@tripetto/block-password";
import "@tripetto/block-radiobuttons";
import "@tripetto/block-rating";
import "@tripetto/block-regex";
import "@tripetto/block-setter";
import "@tripetto/block-text";
import "@tripetto/block-textarea";
import "@tripetto/block-url";
import "@tripetto/block-variable";

/** Import custom blocks */
import "./example";
