import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Text } from "@tripetto/block-text/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-text",
})
export class TextRenderer extends Text implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                <div className={(props.placeholder && "form-floating") || undefined}>
                    <input
                        type="text"
                        className="form-control"
                        id={this.key()}
                        key={this.key()}
                        placeholder={props.placeholder}
                        value={this.textSlot.value}
                        maxLength={this.maxLength}
                        list={(this.props.suggestions && this.key("list")) || undefined}
                        aria-describedby={(props.explanation && this.key("explanation")) || undefined}
                        autoComplete={this.props.autoComplete || "off"}
                        inputMode={this.props.autoComplete === "tel" ? "tel" : "text"}
                        onChange={(ev) => {
                            this.textSlot.value = ev.target.value;
                        }}
                        onFocus={(ev) => {
                            ev.target.classList.remove("is-invalid");
                        }}
                        onBlur={(ev) => {
                            ev.target.value = this.textSlot.string;
                            ev.target.classList.toggle("is-invalid", this.isFailed);
                        }}
                    />
                    {props.placeholder && <label htmlFor={this.key()}>{props.placeholder}</label>}
                </div>
                {this.props.suggestions && (
                    <datalist id={this.key("list")}>
                        {this.props.suggestions.map((suggestion, index) => suggestion && <option key={index} value={suggestion.name} />)}
                    </datalist>
                )}
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
