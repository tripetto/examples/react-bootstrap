import { tripetto, Num } from "@tripetto/runner";
import { ReactNode } from "react";
import { Rating as RatingBlock } from "@tripetto/block-rating/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-rating",
})
export class RatingRenderer extends RatingBlock implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        const buttons: ReactNode[] = [];
        const steps = Num.max(1, this.steps);
        const iconName =
            this.props.shape === "hearts"
                ? "heart"
                : this.props.shape === "persons"
                ? "user"
                : this.props.shape === "stars"
                ? "star"
                : this.props.shape || "star";

        for (let i = 1; i <= steps; i++) {
            const checked = this.ratingSlot.value >= i;

            buttons.push(
                <button
                    key={i}
                    type="button"
                    className="btn btn-link btn-lg px-1 py-0"
                    onClick={() => {
                        this.ratingSlot.value = checked && this.ratingSlot.value === i ? i - 1 : i;

                        if (!this.ratingSlot.value) {
                            this.ratingSlot.clear();
                        }
                    }}
                >
                    <i className={`fa${checked ? "s" : "r"} fa-${iconName}`} />
                </button>
            );
        }

        return (
            <div className="mb-3">
                {this.props.imageURL && this.props.imageAboveText && (
                    <div className="mb-2">
                        <img src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} />
                    </div>
                )}
                {props.name(this.required)}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <div className="mb-2">
                        <img src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} />
                    </div>
                )}
                {buttons}
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
