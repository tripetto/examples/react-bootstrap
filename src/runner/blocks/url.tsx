import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { URL } from "@tripetto/block-url/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-url",
})
export class URLRenderer extends URL implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                <div className={(props.placeholder && "form-floating") || undefined}>
                    <input
                        type="url"
                        className="form-control"
                        id={this.key()}
                        key={this.key()}
                        placeholder={props.placeholder || "https://"}
                        value={this.urlSlot.value}
                        aria-describedby={(props.explanation && this.key("explanation")) || undefined}
                        onChange={(ev) => {
                            this.urlSlot.value = ev.target.value;
                        }}
                        onFocus={(ev) => {
                            ev.target.classList.remove("is-invalid");
                        }}
                        onBlur={(ev) => {
                            ev.target.value = this.urlSlot.string;
                            ev.target.classList.toggle("is-invalid", this.isFailed);
                        }}
                    />
                    {props.placeholder && <label htmlFor={this.key()}>{props.placeholder}</label>}
                </div>
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
