import { ReactNode } from "react";
import { Runner, NodeBlock, isString, castToBoolean, markdownifyToString, markdownifyToURL } from "@tripetto/runner";
import { markdownifyToJSX } from "../markdown";
import { Buttons } from "../buttons";
import { Page } from "../page";
import { Pages } from "../pages";
import { Progressbar } from "../progressbar";

/** Import headless blocks */
import "@tripetto/block-calculator/runner";
import "@tripetto/block-device/runner";
import "@tripetto/block-evaluate/runner";
import "@tripetto/block-hidden-field/runner";
import "@tripetto/block-regex/runner";
import "@tripetto/block-setter/runner";
import "@tripetto/block-variable/runner";

/** Import runner blocks */
import "./checkbox";
import "./checkboxes";
import "./dropdown";
import "./email";
import "./example";
import "./number";
import "./password";
import "./radiobuttons";
import "./rating";
import "./text";
import "./textarea";
import "./url";

export interface IBlockRenderer extends NodeBlock {
    render: (props: IBlockProps) => ReactNode;
}

export interface IBlockProps {
    /** Parsed markdown name. */
    readonly name: (required?: boolean) => ReactNode;

    /** Parsed markdown name as label (single line). */
    readonly label: (required?: boolean) => ReactNode;

    /** Parsed markdown description. */
    readonly description: ReactNode;

    /** Parsed markdown explanation. */
    readonly explanation: ReactNode;

    /** Parsed markdown placeholder. */
    placeholder: string;

    /** Helper function to parse markdown to JSX. */
    readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => JSX.Element;

    /** Helper function to parse markdown to an image URL. */
    readonly markdownifyToImage: (md: string) => string;
}

export class Blocks extends Runner<IBlockRenderer, unknown> {
    render(options: { enumerators: boolean; pages: boolean; progressbar: boolean }): ReactNode {
        const storyline = this.storyline;

        return (
            storyline &&
            !storyline.isEmpty && (
                <>
                    {storyline.map((moment, page) => (
                        <Page key={page} page={page} mode={this.mode} isPreview={this.isPreview} title={moment.section.props.name}>
                            {moment.nodes.map((node) =>
                                node.block ? (
                                    <div key={node.key}>
                                        {node.block.render({
                                            name: (required?: boolean) =>
                                                isString(node.props.name) &&
                                                castToBoolean(node.props.nameVisible, true) && (
                                                    <h3>
                                                        {options.enumerators && node.enumerator && `${node.enumerator}. `}
                                                        {markdownifyToJSX(node.props.name || "...", node.context)}
                                                        {required && <span className="text-danger">*</span>}
                                                    </h3>
                                                ),
                                            label: (required?: boolean) => (
                                                <>
                                                    {markdownifyToJSX(node.props.name || "...", node.context, false)}
                                                    {required && <span className="text-danger">*</span>}
                                                </>
                                            ),
                                            get description(): ReactNode {
                                                return (
                                                    node.props.description && (
                                                        <p className="text-secondary">
                                                            {markdownifyToJSX(node.props.description, node.context)}
                                                        </p>
                                                    )
                                                );
                                            },
                                            get explanation(): ReactNode {
                                                return node.props.explanation && markdownifyToJSX(node.props.explanation, node.context);
                                            },
                                            get placeholder(): string {
                                                return markdownifyToString(node.props.placeholder || "", node.context, "...");
                                            },
                                            markdownifyToJSX: (md: string, lineBreaks?: boolean) =>
                                                markdownifyToJSX(md, node.context, lineBreaks),
                                            markdownifyToImage: (md: string) =>
                                                markdownifyToURL(md, node.context, undefined, [
                                                    "image/jpeg",
                                                    "image/png",
                                                    "image/svg",
                                                    "image/gif",
                                                ]) || "",
                                        })}
                                    </div>
                                ) : (
                                    <div key={node.key}>
                                        {castToBoolean(node.props.nameVisible, true) && (
                                            <h3>{markdownifyToJSX(node.props.name || "...", node.context)}</h3>
                                        )}
                                        {node.props.description && (
                                            <p className="text-secondary">{markdownifyToJSX(node.props.description, node.context, true)}</p>
                                        )}
                                    </div>
                                )
                            )}
                        </Page>
                    ))}
                    {!this.isPreview && (
                        <nav className="row navigation justify-content-between align-items-center mt-3">
                            <Buttons storyline={storyline} />
                            {options.progressbar && <Progressbar percentage={storyline.percentage} />}
                            {options.pages && <Pages pages={storyline.pages} />}
                        </nav>
                    )}
                </>
            )
        );
    }
}
