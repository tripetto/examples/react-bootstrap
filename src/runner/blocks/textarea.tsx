import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Textarea } from "@tripetto/block-textarea/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-textarea",
})
export class TextareaRenderer extends Textarea implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                <textarea
                    className="form-control"
                    id={this.key()}
                    key={this.key()}
                    rows={3}
                    placeholder={props.placeholder}
                    value={this.textareaSlot.value}
                    aria-describedby={(props.explanation && this.key("explanation")) || undefined}
                    onChange={(ev) => {
                        this.textareaSlot.value = ev.target.value;
                    }}
                    onFocus={(ev) => {
                        ev.target.classList.remove("is-invalid");
                    }}
                    onBlur={(ev) => {
                        ev.target.value = this.textareaSlot.string;
                        ev.target.classList.toggle("is-invalid", this.isFailed);
                    }}
                ></textarea>
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
