import { tripetto, castToString } from "@tripetto/runner";
import { ReactNode } from "react";
import { Number } from "@tripetto/block-number/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-number",
})
export class NumberRenderer extends Number implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                <div className={(props.placeholder && "form-floating") || undefined}>
                    <input
                        type="text"
                        className="form-control"
                        id={this.key()}
                        key={this.key()}
                        placeholder={props.placeholder}
                        value={(this.numberSlot.hasValue && this.numberSlot.string) || ""}
                        aria-describedby={(props.explanation && this.key("explanation")) || undefined}
                        onChange={(ev) => {
                            this.numberSlot.pristine = ev.target.value;
                        }}
                        onFocus={(ev) => {
                            const el = ev.target;

                            /** In Firefox we lose focus when switching input type. */
                            requestAnimationFrame(() => {
                                el.focus();
                            });

                            // Switch to number type when focus is gained.
                            el.value = castToString(this.numberSlot.value);
                            el.type = "number";
                            el.classList.remove("is-invalid");
                        }}
                        onBlur={(ev) => {
                            const el = ev.target;

                            // Switch to text type to allow number prefix and suffix.
                            el.type = "text";
                            el.value = this.numberSlot.string;
                            el.classList.toggle("is-invalid", this.isFailed);
                        }}
                    />
                    {props.placeholder && <label htmlFor={this.key()}>{props.placeholder}</label>}
                </div>
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
