import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Checkbox } from "@tripetto/block-checkbox/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-checkbox",
})
export class CheckboxRenderer extends Checkbox implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.placeholder && props.name(this.required)}
                {props.description}
                <div className="form-check form-switch">
                    <input
                        className="form-check-input"
                        type="checkbox"
                        checked={this.checkboxSlot.value}
                        onChange={(ev) => {
                            this.checkboxSlot.value = ev.target.checked;
                        }}
                        role="switch"
                        id={this.key()}
                    />
                    <label className="form-check-label" htmlFor={this.key()}>
                        {props.placeholder || props.label(this.required)}
                    </label>
                </div>
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
