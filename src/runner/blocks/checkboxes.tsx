import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Checkboxes, ICheckbox } from "@tripetto/block-checkboxes/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-checkboxes",
})
export class CheckboxesRenderer extends Checkboxes implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                {this.checkboxes(props).map((checkbox) => (
                    <div className="form-check" key={this.key(checkbox.id)}>
                        <input
                            className="form-check-input"
                            type="checkbox"
                            id={this.key(checkbox.id)}
                            checked={this.isChecked(checkbox)}
                            onChange={() => this.toggle(checkbox)}
                        />
                        <label className="form-check-label" htmlFor={this.key(checkbox.id)}>
                            {checkbox.label || "..."}
                        </label>
                    </div>
                ))}
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
