import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Dropdown } from "@tripetto/block-dropdown/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-dropdown",
})
export class DropdownRenderer extends Dropdown implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                <select
                    className="form-select"
                    id={this.key()}
                    key={this.key()}
                    aria-describedby={(props.explanation && this.key("explanation")) || undefined}
                    onChange={(ev) => {
                        this.value = ev.target.value;
                    }}
                    onFocus={(ev) => {
                        ev.target.classList.remove("is-invalid");
                    }}
                    onBlur={(ev) => {
                        ev.target.classList.toggle("is-invalid", this.isFailed);
                    }}
                >
                    {props.placeholder && <option>{props.placeholder}</option>}
                    {this.options.map(
                        (option) =>
                            option.name && (
                                <option key={option.id} value={option.id} selected={this.value === option.id}>
                                    {option.name}
                                </option>
                            )
                    )}
                </select>
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
