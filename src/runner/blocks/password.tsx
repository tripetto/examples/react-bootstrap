import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Password } from "@tripetto/block-password/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-password",
})
export class PasswordRenderer extends Password implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                <div className={(props.placeholder && "form-floating") || undefined}>
                    <input
                        type="password"
                        className="form-control"
                        id={this.key()}
                        key={this.key()}
                        placeholder={props.placeholder}
                        value={this.passwordSlot.value}
                        aria-describedby={(props.explanation && this.key("explanation")) || undefined}
                        onChange={(ev) => {
                            this.passwordSlot.value = ev.target.value;
                        }}
                        onFocus={(ev) => {
                            ev.target.classList.remove("is-invalid");
                        }}
                        onBlur={(ev) => {
                            ev.target.value = this.passwordSlot.string;
                            ev.target.classList.toggle("is-invalid", this.isFailed);
                        }}
                    />
                    {props.placeholder && <label htmlFor={this.key()}>{props.placeholder}</label>}
                </div>
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
