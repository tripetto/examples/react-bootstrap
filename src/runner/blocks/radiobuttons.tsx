import { tripetto } from "@tripetto/runner";
import { ReactNode } from "react";
import { Radiobuttons } from "@tripetto/block-radiobuttons/runner";
import { IBlockRenderer, IBlockProps } from ".";

@tripetto({
    type: "node",
    identifier: "@tripetto/block-radiobuttons",
})
export class RadiobuttonsRenderer extends Radiobuttons implements IBlockRenderer {
    render(props: IBlockProps): ReactNode {
        return (
            <div className="mb-3">
                {props.name(this.required)}
                {props.description}
                {this.buttons(props).map((radiobutton) => (
                    <div className="form-check" key={this.key(radiobutton.id)}>
                        <input
                            className="form-check-input"
                            type="radio"
                            name={this.key()}
                            id={this.key(radiobutton.id)}
                            checked={this.value === radiobutton.id}
                            onChange={(ev) => {
                                if (ev.target.checked) {
                                    this.value = radiobutton.id;
                                }
                            }}
                        />
                        <label className="form-check-label" htmlFor={this.key(radiobutton.id)}>
                            {radiobutton.name}
                        </label>
                    </div>
                ))}
                {props.explanation && (
                    <div id={this.key("explanation")} className="form-text">
                        {props.explanation}
                    </div>
                )}
            </div>
        );
    }
}
