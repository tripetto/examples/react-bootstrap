import { ReactNode } from "react";
import { L10n, TModes } from "@tripetto/runner";
import "./page.scss";

export const Page = (props: {
    readonly page: number;
    readonly mode: TModes;
    readonly title?: string;
    readonly isPreview: boolean;
    readonly children?: ReactNode;
}) =>
    props.isPreview ? (
        <>
            <div className="page mb-3">
                <div></div>
                <div>
                    {props.mode === "paginated" ? "Page" : "Section"} {L10n.Locales.number(props.page + 1)}
                    {(props.title && ` - ${props.title}`) || ""}
                </div>
                <div></div>
            </div>
            {props.children}
        </>
    ) : (
        <>{props.children}</>
    );
