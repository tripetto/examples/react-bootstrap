export const Progressbar = (props: { percentage: number }) => (
    <div className="col pb-3 pb-md-0">
        <div className="progress">
            <div
                className="progress-bar bg-primary"
                role="progressbar"
                aria-valuenow={props.percentage}
                aria-valuemin={0}
                aria-valuemax={100}
                style={{ width: `${props.percentage}%`, minWidth: "2em" }}
            >
                {props.percentage}%
            </div>
        </div>
    </div>
);
