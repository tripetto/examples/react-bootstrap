import { PureComponent, ReactNode } from "react";
import { Export, IDefinition, ISnapshot, TModes } from "@tripetto/runner";
import { Blocks } from "./blocks";

export class BootstrapRunner extends PureComponent<{
    definition: IDefinition;
    snapshot?: ISnapshot;
    mode?: TModes;
    preview?: boolean;
    enumerators?: boolean;
    pages?: boolean;
    progressbar?: boolean;
    onSubmit?: (data: Export.IExportables) => void;
    onPause?: (data: ISnapshot) => void;
}> {
    readonly blocks = new Blocks({
        definition: this.props.definition,
        snapshot: this.props.snapshot,
        mode: this.props.mode || "paginated",
        preview: this.props.preview,
        start: true,
    });

    onChange?: () => void;

    render(): ReactNode {
        if (this.props.mode && this.props.mode !== this.blocks.mode) {
            this.blocks.mode = this.props.mode;
        }

        return (
            <section>
                <div>
                    {this.blocks.render({
                        enumerators: this.props.enumerators || false,
                        pages: this.props.pages || false,
                        progressbar: this.props.progressbar || false,
                    }) ||
                        ((this.blocks.status === "empty" || this.blocks.status === "preview") && (
                            <div className="row justify-content-center align-items-center message">
                                <div className="col-md-8 col-lg-10">
                                    <div className="text-center">
                                        <h2>👋 Nothing to show here yet</h2>
                                        <p className="text-faded">Add blocks to the form first to get the magic going.</p>
                                    </div>
                                </div>
                            </div>
                        )) ||
                        (this.blocks.status === "finished" && (
                            <div className="row justify-content-center align-items-center message">
                                <div className="col-md-8 col-lg-10">
                                    <div className="text-center">
                                        <h2>✅ You’ve completed the form</h2>
                                        <p className="text-faded">
                                            For the purpose of this demo the form output is visible in your browser’s developer console. Hit{" "}
                                            <b>F12</b> to go there and see the data collected by this form.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        )) ||
                        (this.blocks.status === "stopped" && (
                            <div className="row justify-content-center align-items-center message">
                                <div className="col-md-8 col-lg-10">
                                    <div className="text-center">
                                        <h2>⏹ You’ve stopped the form</h2>
                                        <p className="text-faded">Press the play icon to start a new session.</p>
                                    </div>
                                </div>
                            </div>
                        )) ||
                        (this.blocks.status === "paused" && (
                            <div className="row justify-content-center align-items-center message">
                                <div className="col-md-8 col-lg-10">
                                    <div className="text-center">
                                        <h2>⏸ You’ve paused the form</h2>
                                        <p className="text-faded">
                                            For the purpose of this demo the paused form is saved in your browser’s local store. Refresh the
                                            browser to resume the paused form.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        )) || (
                            <div className="row justify-content-center align-items-center message">
                                <div className="col-md-8 col-lg-10">
                                    <div className="text-center">
                                        <h2>⏹ You haven’t started the form yet</h2>
                                        <p className="text-faded">Press the play icon to start a new session.</p>
                                    </div>
                                </div>
                            </div>
                        )}
                </div>
            </section>
        );
    }

    componentDidMount(): void {
        this.blocks.onChange = () => {
            // Since the runner has the actual state, we need to update the component when the runner state changes.
            // We are good React citizens. We only do this when necessary!
            this.forceUpdate();

            if (this.onChange) {
                this.onChange();
            }
        };

        this.blocks.onFinish = (instance) => {
            if (this.props.onSubmit) {
                this.props.onSubmit(Export.exportables(instance));
            }

            return true;
        };
    }

    componentWillUnmount(): void {
        this.blocks.destroy();
    }

    /** Start the runner. */
    start(): void {
        this.blocks.start();
    }

    /** Pauses the runner. */
    pause(): ISnapshot | undefined {
        const snapshot = this.blocks.pause();

        if (snapshot && this.props.onPause) {
            this.props.onPause(snapshot);
        }

        return snapshot;
    }

    /** Stop the runner. */
    stop(): void {
        this.blocks.stop();
    }

    /** Resets the runner. */
    reset(): void {
        this.blocks.restart(false);
    }

    /** Reload the runner with a new definition. */
    reload(definition: IDefinition): void {
        this.blocks.reload(definition);
    }
}
