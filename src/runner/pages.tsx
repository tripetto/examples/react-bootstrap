import { IPage } from "@tripetto/runner";

export const Pages = (props: { pages: IPage[] }) =>
    (props.pages.length > 0 && (
        <div className="col-md-auto pb-3 pb-md-0">
            <div className="btn-group mb-0" role="group">
                {props.pages.map((page) => (
                    <button
                        key={page.number}
                        type="button"
                        className={`btn btn-normal btn-outline-primary${page.active ? " active" : ""}`}
                        onClick={() => page.activate()}
                    >
                        {page.number}
                    </button>
                ))}
            </div>
        </div>
    )) || <></>;
