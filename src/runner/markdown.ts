import { createElement } from "react";
import {
    Context,
    Enumerator,
    IVariable,
    MarkdownFeatures,
    MarkdownTypes,
    castToString,
    each,
    isString,
    markdownifyTo,
} from "@tripetto/runner";

const KEY = new Enumerator();

/**
 * Parses markdown to JSX.
 * @param md Specifies the markdown string to parse.
 * @param context Reference to the context.
 * @param lineBreaks Specifies if line breaks should be emitted (default is `true`).
 * @param features Specifies the markdown features to parse (defaults to `MarkdownFeatures.Formatting | MarkdownFeatures.Hyperlinks`).
 */
export function markdownifyToJSX(
    md: string,
    context?: Context,
    lineBreaks: boolean = true,
    features?: MarkdownFeatures,
    variables?: string
): JSX.Element {
    return markdownifyTo(md, context, {
        lineBreaks,
        features,
        reduce: (type: MarkdownTypes | undefined, content: string | JSX.Element[], value?: IVariable | string) => {
            const children = isString(content) ? [content] : content;

            switch (type) {
                case "bold":
                    return createElement("b", undefined, ...children);
                case "italic":
                    return createElement("i", undefined, ...children);
                case "bold+italic":
                    return createElement("b", undefined, createElement("i", undefined, ...children));
                case "underline":
                    return createElement("u", undefined, ...children);
                case "strikethrough":
                    return createElement("s", undefined, ...children);
                case "break":
                    return createElement("br");
                case "hyperlink":
                    return createElement(
                        "a",
                        {
                            href: castToString(value),
                            target: "_blank",
                            rel: "noopener",
                        },
                        ...children
                    );
                case "mention":
                    const variable = (value && (value as IVariable)) || undefined;

                    if (variable && variable.content.length > 0) {
                        const elements: JSX.Element[] = [];

                        each(variable.content, (element) => {
                            const elementValue = element.value;

                            if (elementValue) {
                                if (elements.length > 0 && element.separator) {
                                    elements.push(createElement("span", undefined, element.separator));
                                }

                                if (element.type === "string") {
                                    elements.push(
                                        lineBreaks && elementValue && elementValue.indexOf("\n") !== -1
                                            ? markdownifyToJSX(elementValue.replace(/[@\\]/g, "\\$&"), context, true, MarkdownFeatures.None)
                                            : createElement(elementValue ? "span" : "u", undefined, elementValue || "\u00A0\u00A0\u00A0")
                                    );
                                } else if (!variables || variables.indexOf("@" + variable.id) === -1) {
                                    elements.push(
                                        markdownifyToJSX(
                                            elementValue,
                                            context,
                                            lineBreaks,
                                            element.type === "text"
                                                ? MarkdownFeatures.None
                                                : MarkdownFeatures.Formatting | MarkdownFeatures.Hyperlinks,
                                            (variables || "") + "@" + variable.id
                                        )
                                    );
                                }
                            }
                        });

                        if (elements.length > 0) {
                            return createElement("span", undefined, ...elements);
                        } else {
                            return createElement("u", undefined, "\u00A0\u00A0\u00A0");
                        }
                    } else {
                        return createElement("u", undefined, "\u00A0\u00A0\u00A0");
                    }
            }

            return createElement("span", { key: KEY.n }, ...children);
        },
    });
}
