import { Storyline } from "@tripetto/runner";
import { IBlockRenderer } from "./blocks";

export const Buttons = (props: { storyline: Storyline<IBlockRenderer> }) => (
    <div className="col-md-auto pb-3 pb-md-0">
        <div role="group" aria-label="Navigation">
            {props.storyline.mode === "progressive" ? (
                <button
                    type="button"
                    className="btn btn-success"
                    disabled={!props.storyline.isFinishable}
                    onClick={() => props.storyline.finish()}
                >
                    <i className="fas fa-check fa-fw me-2" />
                    Complete
                </button>
            ) : (
                <>
                    <button
                        type="button"
                        className={`btn btn-${props.storyline.isAtFinish ? "success" : "primary"}`}
                        disabled={props.storyline.isFailed || (props.storyline.isAtFinish && !props.storyline.isFinishable)}
                        onClick={() => props.storyline.stepForward()}
                    >
                        <i className={`fas fa-${props.storyline.isAtFinish ? "check" : "chevron-right"} fa-fw me-2`} />
                        {props.storyline.isAtFinish ? "Complete" : "Next"}
                    </button>
                    {!props.storyline.isAtStart && (
                        <button
                            type="button"
                            className="btn btn-light ms-2"
                            disabled={props.storyline.isAtStart}
                            onClick={() => props.storyline.stepBackward()}
                        >
                            <i className="fas fa-chevron-left fa-fw me-2" />
                            Back
                        </button>
                    )}
                </>
            )}
        </div>
    </div>
);
