import { RefObject, MutableRefObject, useEffect, useState } from "react";
import { BootstrapRunner } from "../runner/runner";
import { Builder } from "@tripetto/builder";
import { TModes } from "@tripetto/runner";
import { SettingsDialog } from "./settings";
import "./header.scss";

const HeaderLinks = (props: { readonly isDropdown?: boolean; readonly restore: () => void }) => (
    <>
        <a
            href="https://gitlab.com/tripetto/examples/react-bootstrap"
            target="_blank"
            role="button"
            className={`btn btn-sm btn-link${props.isDropdown ? " dropdown-item" : ""}`}
        >
            <i className="fas fa-code fa-fw me-2" />
            Get source
        </a>
        <a
            href="https://gitlab.com/tripetto/examples/react-bootstrap/blob/main/README.md"
            target="_blank"
            role="button"
            className={`btn btn-sm btn-link${props.isDropdown ? " dropdown-item" : ""}`}
        >
            <i className="fas fa-book-reader fa-fw me-2" />
            View readme
        </a>
        <a
            href="#"
            onClick={() => props.restore()}
            role="button"
            className={`btn btn-sm btn-link${props.isDropdown ? " dropdown-item" : ""}`}
        >
            <i className="fas fa-backward-step fa-fw me-2" />
            Restore demo
        </a>
    </>
);

const HeaderControls = (props: {
    readonly isDropdown?: boolean;
    readonly runner: RefObject<BootstrapRunner>;
    readonly reset: () => void;
}) => (
    <>
        <div className={`btn-group ${props.isDropdown ? "dropdown-item" : "me-2"}`} role="group" aria-label="Controls">
            <button
                type="button"
                className="btn btn-primary"
                disabled={
                    props.runner.current?.blocks.isPreview ||
                    props.runner.current?.blocks.isEmpty ||
                    (!props.runner.current?.blocks.isStopped && !props.runner.current?.blocks.isFinished)
                }
                title="Start form"
                onClick={() => props.runner.current?.start()}
            >
                <i className="fas fa-play fa-fw" />
            </button>
            <button
                type="button"
                className="btn btn-primary"
                disabled={props.runner.current?.blocks.isPreview || !props.runner.current?.blocks.isRunning}
                title="Pause form"
                onClick={() => props.runner.current?.pause()}
            >
                <i className="fas fa-pause fa-fw" />
            </button>
            <button
                type="button"
                className="btn btn-primary"
                disabled={props.runner.current?.blocks.isPreview || !props.runner.current?.blocks.isRunning}
                title="Stop form"
                onClick={() => props.runner.current?.stop()}
            >
                <i className="fas fa-stop fa-fw" />
            </button>
            <button
                type="button"
                className="btn btn-primary"
                disabled={props.runner.current?.blocks.isPreview || !props.runner.current?.blocks.isRunning}
                title="Reset form"
                onClick={() => props.reset()}
            >
                <i className="fas fa-redo fa-fw" />
            </button>
        </div>

        <div className={`btn-group btn-group-toggle${props.isDropdown ? " dropdown-item" : ""}`} data-toggle="buttons">
            <button
                type="button"
                className={`btn btn-primary${!props.runner.current?.blocks.isPreview ? " active" : ""}`}
                disabled={!props.runner.current?.blocks.isRunning}
                data-toggle="button"
                aria-pressed={!props.runner.current?.blocks.isPreview}
                onClick={() => {
                    if (props.runner.current?.blocks) {
                        props.runner.current.blocks.isPreview = false;
                    }
                }}
            >
                Run
            </button>
            <button
                type="button"
                className={`btn btn-primary${props.runner.current?.blocks.isPreview ? " active" : ""}`}
                disabled={!props.runner.current?.blocks.isRunning}
                data-toggle="button"
                aria-pressed={props.runner.current?.blocks.isPreview}
                onClick={() => {
                    if (props.runner.current?.blocks) {
                        props.runner.current.blocks.isPreview = true;
                    }
                }}
            >
                Preview
            </button>
        </div>

        <button
            type="button"
            className={`btn btn-primary ${props.isDropdown ? "dropdown-item" : "ms-2"}`}
            data-bs-toggle="modal"
            data-bs-target="#settingsModal"
        >
            <i className={`fas fa-cog fa-fw${props.isDropdown ? " me-2" : ""}`} />
            {props.isDropdown && "Settings"}
        </button>
    </>
);

export function Header(props: {
    readonly builder: MutableRefObject<Builder | undefined>;
    readonly runner: RefObject<BootstrapRunner>;
    readonly enumerators: boolean;
    readonly pages: boolean;
    readonly progressbar: boolean;
    readonly setMode: (mode: TModes) => void;
    readonly setEnumerators: (enumerators: boolean) => void;
    readonly setPages: (pages: boolean) => void;
    readonly setProgressbar: (progressbar: boolean) => void;
    readonly reset: () => void;
    readonly restore: () => void;
    readonly builderVisible: boolean;
    readonly setBuilderVisible: (show: boolean) => void;
}) {
    const [, updateHeader] = useState({});

    useEffect(() => {
        if (props.runner.current) {
            props.runner.current.onChange = () => updateHeader({});
        }
    });

    return (
        <>
            <nav id="header" className="navbar bg-light">
                <div className="container-fluid pe-0">
                    <div className="row">
                        <div className="col-9 col-lg-8 d-flex align-items-center">
                            <button
                                type="button"
                                className={`btn me-2 d-lg-none btn-primary`}
                                title={props.builderVisible ? "Hide the builder" : "Show the builder"}
                                onClick={() => props.setBuilderVisible(!props.builderVisible)}
                            >
                                <i className={`fas fa-${props.builderVisible ? "close" : "edit"} fa-fw`} />
                            </button>
                            <h1
                                className={`me-1 me-md-3 text-truncate${
                                    !props.runner.current?.blocks.definition?.name ? " opacity-25" : ""
                                }`}
                                onClick={() => props.builder.current?.edit()}
                            >
                                {props.runner.current?.blocks.definition?.name || "Unnamed form"}
                            </h1>
                            <div className="d-none d-lg-block text-nowrap">
                                <HeaderLinks {...props} />
                            </div>
                        </div>
                        <div className="col-3 col-lg-4 d-flex justify-content-end align-items-center pe-0">
                            <div className="d-none d-lg-block">
                                <HeaderControls {...props} />
                            </div>
                            <div className="d-lg-none">
                                <button
                                    className="btn btn-secondary dropdown-toggle"
                                    type="button"
                                    id="dropdownMenu"
                                    data-bs-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >
                                    <i className="fas fa-cog fa-fw me-1" />
                                </button>
                                <div className="dropdown-menu dropdown-menu-dark dropdown-menu-end" aria-labelledby="dropdownMenu">
                                    <HeaderLinks {...props} isDropdown={true} />
                                    <div className="dropdown-divider" />
                                    <HeaderControls {...props} isDropdown={true} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            {props.runner.current && <SettingsDialog {...props} mode={props.runner.current.blocks.mode} />}
        </>
    );
}
