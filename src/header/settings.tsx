import { TModes } from "@tripetto/runner";

export const SettingsDialog = (props: {
    readonly mode: TModes;
    readonly enumerators: boolean;
    readonly pages: boolean;
    readonly progressbar: boolean;
    readonly setMode: (mode: TModes) => void;
    readonly setEnumerators: (enumerators: boolean) => void;
    readonly setPages: (pages: boolean) => void;
    readonly setProgressbar: (progressbar: boolean) => void;
}) => (
    <div
        className="modal fade"
        id="settingsModal"
        role="dialog"
        data-bs-backdrop="static"
        aria-labelledby="settingsModalTitle"
        aria-hidden="true"
    >
        <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h1 className="modal-title fs-5" id="settingsModalTitle">
                        Runner settings
                    </h1>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <h6>Mode</h6>
                    <div className="form-group mt-2">
                        <div className="mb-2 form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="mode"
                                id="paginated"
                                checked={props.mode === "paginated"}
                                onChange={(ev) => {
                                    if (ev.target.checked) {
                                        props.setMode("paginated");
                                    }
                                }}
                            />
                            <label className="form-check-label" htmlFor="paginated">
                                Paginated
                            </label>
                        </div>
                        <div className="mb-2 form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="mode"
                                id="continuous"
                                checked={props.mode === "continuous"}
                                onChange={(ev) => {
                                    if (ev.target.checked) {
                                        props.setMode("continuous");
                                    }
                                }}
                            />
                            <label className="form-check-label" htmlFor="continuous">
                                Continuous
                            </label>
                        </div>
                        <div className="mb-2 form-check">
                            <input
                                className="form-check-input"
                                type="radio"
                                name="mode"
                                id="progressive"
                                checked={props.mode === "progressive"}
                                onChange={(ev) => {
                                    if (ev.target.checked) {
                                        props.setMode("progressive");
                                    }
                                }}
                            />
                            <label className="form-check-label" htmlFor="progressive">
                                Progressive
                            </label>
                        </div>
                        <hr />
                        <h6>Display</h6>
                        <div className="form-group mt-2">
                            <div className="form-check form-switch mb-2">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    checked={props.enumerators}
                                    onChange={(ev) => props.setEnumerators(ev.target.checked)}
                                    role="switch"
                                    id="enumerators"
                                />
                                <label className="form-check-label" htmlFor="enumerators">
                                    Enumerators
                                </label>
                            </div>
                            <div className="form-check form-switch mb-2">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    checked={props.pages}
                                    disabled={props.mode !== "paginated"}
                                    onChange={(ev) => props.setPages(ev.target.checked)}
                                    role="switch"
                                    id="pages"
                                />
                                <label className="form-check-label" htmlFor="pages">
                                    Page navigation
                                </label>
                            </div>
                            <div className="form-check form-switch mb-2">
                                <input
                                    className="form-check-input"
                                    type="checkbox"
                                    checked={props.progressbar}
                                    onChange={(ev) => props.setProgressbar(ev.target.checked)}
                                    role="switch"
                                    id="progressbar"
                                />
                                <label className="form-check-label" htmlFor="progressbar">
                                    Progressbar
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
