import { useState, useRef, useEffect } from "react";
import { createRoot } from "react-dom/client";
import { IDefinition, Builder } from "@tripetto/builder";
import { TripettoBuilder } from "@tripetto/builder/react";
import { TModes, ISnapshot } from "@tripetto/runner";
import { BootstrapRunner } from "./runner/runner";
import { Header } from "./header/header";
import DEMO_FORM from "./demo.json";
import "./app.scss";
import "bootstrap";

/** Import builder blocks */
import "./builder/blocks";

const DEFINITION_KEY = "@tripetto/example-react-bootstrap-definition";
const DEFINTIION = (JSON.parse(localStorage.getItem(DEFINITION_KEY) || "null") || DEMO_FORM) as IDefinition;

const SNAPSHOT_KEY = "@tripetto/example-react-bootstrap-snapshot";
const SNAPSHOT = (JSON.parse(localStorage.getItem(SNAPSHOT_KEY) || "null") || undefined) as ISnapshot;

function App() {
    const [ready, setReady] = useState(false);
    const [mode, setMode] = useState<TModes>("paginated");
    const [enumerators, setEnumerators] = useState(false);
    const [pages, setPages] = useState(false);
    const [progressbar, setProgressbar] = useState(true);
    const [builderVisible, setBuilderVisible] = useState(false);
    const runnerRef = useRef<BootstrapRunner>(null);
    const builderRef = useRef<Builder>();

    useEffect(() => {
        // Only show the tutorial once
        localStorage.setItem("tripetto-tutorial", "hide");
    });

    return (
        <div style={{ visibility: ready ? "visible" : "hidden" }}>
            <Header
                builder={builderRef}
                runner={runnerRef}
                reset={() => {
                    runnerRef.current?.reset();

                    localStorage.removeItem(SNAPSHOT_KEY);
                }}
                restore={() => {
                    builderRef.current?.load(DEMO_FORM as {} as IDefinition);
                    runnerRef.current?.reload(DEMO_FORM as {} as IDefinition);

                    localStorage.removeItem(DEFINITION_KEY);
                    localStorage.removeItem(SNAPSHOT_KEY);
                }}
                {...{
                    setMode,
                    setEnumerators,
                    setPages,
                    setProgressbar,
                    enumerators,
                    pages,
                    progressbar,
                    builderVisible: builderVisible,
                    setBuilderVisible: setBuilderVisible,
                }}
            />
            <div id="builder" className={(builderVisible && "visible") || ""}>
                <TripettoBuilder
                    controller={builderRef}
                    definition={DEFINTIION}
                    fonts="assets/"
                    disableSaveButton={true}
                    disableRestoreButton={true}
                    disableClearButton={false}
                    disableCloseButton={true}
                    supportURL={false}
                    disableOpenCloseAnimation={true}
                    disableEpilogue={true}
                    disablePrologue={true}
                    showTutorial={!localStorage.getItem("tripetto-tutorial")}
                    zoom="fit"
                    onReady={() => setReady(true)}
                    onChange={(changedDefinition) => {
                        runnerRef.current?.reload(changedDefinition);

                        // Store the definition in the persistent local store
                        localStorage.setItem(DEFINITION_KEY, JSON.stringify(changedDefinition));
                    }}
                />
            </div>
            <div id="runner">
                <div className="alert alert-info" role="alert">
                    💡 To keep this demo as simple as possible, it only contains a selection of the{" "}
                    <a href="https://tripetto.com/sdk/docs/blocks/stock/" target="_blank">
                        stock blocks
                    </a>{" "}
                    available.
                </div>
                <div className="alert alert-warning" role="alert">
                    ⚠️ This is a demonstration. No form data will ever be sent. Form data is shown in the browser console.
                </div>
                <BootstrapRunner
                    ref={runnerRef}
                    definition={DEFINTIION}
                    snapshot={SNAPSHOT}
                    mode={mode}
                    enumerators={enumerators}
                    pages={pages}
                    progressbar={progressbar}
                    onSubmit={(data) => {
                        // Output the collected data to the console
                        console.log("Form completed!");

                        data.fields.forEach((field) => {
                            if (field.string) {
                                console.log(`${field.name}: ${field.string}`);
                            }
                        });
                    }}
                    onPause={(data) => {
                        // Store the pause snapshot in the persistent local store
                        localStorage.setItem(SNAPSHOT_KEY, JSON.stringify(data));
                    }}
                />
            </div>
        </div>
    );
}

// Let's render the App component to the root element.
createRoot(document.getElementById("root")!).render(<App />);
